#use "semantic-analyser.ml";;
open Semantics;;
open Tag_Parser;;
open Reader;;

exception X_syntax_error;;

module type CODE_GEN = sig
  val make_consts_tbl : expr' list -> (constant * (int * string)) list (*TODO:   val make_consts_tbl : expr' list -> (constant * ('a * string)) list *)
  val make_fvars_tbl : expr' list -> (string * (int * string)) list           (*TODO:   val make_fvars_tbl : expr' list -> (string * 'a) list*)
  val generate : (constant * (int * string)) list -> (string * (int * string)) list -> expr' -> string
end;;

module Code_Gen : CODE_GEN = struct

   (*two functions that removing_duplicates*)
  let label_generator = ref (-1);;
  let label_maker label =  label_generator := !label_generator + 1 ;
                           Printf.sprintf("%s%d")  label !label_generator;;

  let single_element_compare tl hd = if List.mem hd tl then tl else hd::tl 
  let removing_duplicates tbl_const = List.rev (List.fold_left single_element_compare [] tbl_const)

  let assembly_strings sexpr index = (* index only for debug *)
    match sexpr with 
      | Sexpr(Number(Int x))      -> Printf.sprintf("MAKE_LITERAL_INT(%d) ;offset is %d") x index
      | Sexpr(Number(Float x))    -> Printf.sprintf("MAKE_LITERAL_INT(%f ;offset is %d") x index 
      | Sexpr(Bool(false))        -> "MAKE_BOOL(0)"
      | Sexpr(Bool(true))         -> "MAKE_BOOL(1)"
      | Sexpr(String(x))          -> Printf.sprintf("MAKE_LITERAL_STRING %s ;offset is %d") x index 
      | Sexpr(Char(x))            -> Printf.sprintf("MAKE_LITERAL_CHAR(%c ;offset is %d") x index 
      | Sexpr(Nil)                -> "MAKE_NIL"
      | Void                      -> "MAKE_VOID" 
      | Sexpr(Vector(list))       -> "MAKE_LITERAL_VECTOR" 
      | _                         -> "MAKE_Dummy" (*TODO:VECTOR*)


  let rec size_of_the_constant sexpr = 
    match sexpr with
      | Sexpr(Number(x))          -> 9
      | Sexpr(Bool(x))            -> 2
      | Sexpr(String(x))          -> 9 + (String.length x)
      | Sexpr(Symbol(x))          -> 9
      | Sexpr(Char(x))            -> 2
      | Sexpr(Nil)                -> 1
      | Sexpr(Vector(list))       -> 9 + (8 * List.length list)
      | Void                      -> 1 
      | Sexpr(Pair(car,cdr))      -> 17

  (* let rec populate_sub_constants_table final_tbl const_tbl const_copy_tbl=  
    match const_tbl with
      | (Sexpr(Symbol(x), (i,_)))         -> let (offset,_) = List.assoc (String(x)) const_tbl
                                             (Sexpr(Symbol(x), (i,Printf.sprintf("MAKE_LITERAL_SYMBOL(consts+%d)") offset ))) :: final_tbl
      | _                                 -> _ :: final_tbl *)

  let rec collect_the_sexprs asts tbl = 
   match asts with
      | Const' (exp)                      -> tbl@[exp]
      | BoxSet'(var,exp)                  -> collect_the_sexprs exp tbl
      | BoxGet'   _                       -> tbl
      | Box'      _                       -> tbl
      | Seq' (exps)                       -> let seq_tbl  = List.rev (List.fold_right (fun exp -> collect_the_sexprs exp) exps []) in
                                             tbl @ seq_tbl
      | If' (test,dit,dif)                -> let test_tbl = collect_the_sexprs test tbl      in
                                             let dit_tbl  = collect_the_sexprs dit test_tbl  in
                                             let dif_tbl  = collect_the_sexprs dif dit_tbl   in
                                             dif_tbl
      | Def' (var,exp)                    -> collect_the_sexprs exp tbl
      | Or' (exps)                        -> let or_tbl  = List.rev (List.fold_right (fun exp -> collect_the_sexprs exp) exps []) in
                                             tbl @ or_tbl
      | Applic' (op, rands)               -> let op_tbl = collect_the_sexprs op tbl in
                                             let applic_tbl =  List.rev (List.fold_right (fun exp -> collect_the_sexprs exp) rands []) in
                                             op_tbl @ applic_tbl
      | ApplicTP' (op, rands)             -> let op_tbl = collect_the_sexprs op tbl in 
                                             let applic_tbl = List.rev (List.fold_right (fun exp -> collect_the_sexprs exp) rands []) in 
                                             op_tbl @ applic_tbl
      | Var'(var)                         -> tbl
      | Set' (var,exp)                    -> collect_the_sexprs exp tbl
      | LambdaSimple'(vars, body)         -> collect_the_sexprs body tbl
      | LambdaOpt'(vars,opt_vals, body)   -> collect_the_sexprs body tbl

  let rec serch_for_all_sub_constants tbl_set = 
  	List.fold_left 
		(fun acc_list sexpr -> acc_list@serch_for_all_sub_constants_help sexpr acc_list) 
		[]
		tbl_set
	        									
  and serch_for_all_sub_constants_help sexpr acc_list = 
	  match sexpr with
      | Sexpr(Symbol exp)         -> 	Sexpr(String exp)::[sexpr]

      | Sexpr(Pair(car,cdr))      -> 	(serch_for_all_sub_constants_help (Sexpr(car)) acc_list)
                                @ 	(serch_for_all_sub_constants_help (Sexpr(cdr)) acc_list)
                                @	[sexpr]
                              
      | Sexpr(Vector(exprs))      ->  (List.fold_left 
                        (fun acc_list exp -> 
                          acc_list @serch_for_all_sub_constants_help (Sexpr(exp)) acc_list 
                        )
                        acc_list
                        exprs) @[sexpr]
      | _-> [sexpr]    

  let rec make_tupple_constants_table index tbl tbl_copy= 
    match tbl with 
      | []       -> []
      | hd::tail -> [(hd,(index,assembly_strings hd index))] @ make_tupple_constants_table (size_of_the_constant hd + index) tail tbl_copy
  
  let populate_constants_table tbl = make_tupple_constants_table 0 tbl ;;

  let rec make_final_tupple const_tbl origion_const_tbl=  
    match const_tbl with
      | []                                    -> []
      | (Sexpr(Symbol(x)), (i,_))::tail       -> let (offset,_) = List.assoc (Sexpr(String(x))) origion_const_tbl in
                                                 let ass_string = Printf.sprintf "MAKE_LITERAL_SYMBOL(consts+%d)" offset in
                                                 [(Sexpr(Symbol(x)), (i,ass_string))]  @ make_final_tupple tail origion_const_tbl
      | (Sexpr(Pair(car,cdr)), (i,_))::tail   -> let (offset_car,_) = List.assoc (Sexpr(car)) origion_const_tbl in
                                                 let (offset_cdr,_) = List.assoc (Sexpr(cdr)) origion_const_tbl in
                                                 let ass_string = Printf.sprintf "MAKE_LITERAL_SYMBOL(consts+%d,consts+%d)" offset_car offset_cdr in
                                                 [(Sexpr(Pair(car,cdr)), (i,ass_string))]  @ make_final_tupple tail origion_const_tbl
      (* | (Sexpr(Vector(x)), (i,_))::tail       -> TODO:VECTOR SIZE                                                                                   *)
      | hd::tail                              -> hd :: make_final_tupple tail origion_const_tbl


  let rec getting_free_vars asts tbl = 
   match asts with
      | Const' (exp)                      -> []
      | BoxSet'(var,exp)                  -> getting_free_vars exp tbl
      | BoxGet' _                         -> []
      | Box'    _                         -> []
      | Seq' (exps)                       -> let seq_tbl  = List.rev (List.fold_right (fun exp -> getting_free_vars exp) exps []) in
                                             tbl @ seq_tbl
      | If' (test,dit,dif)                -> let test_tbl = getting_free_vars test tbl      in
                                             let dit_tbl  = getting_free_vars dit test_tbl  in
                                             let dif_tbl  = getting_free_vars dif dit_tbl   in
                                             dif_tbl
      | Def' (var,exp)                    -> getting_free_vars exp tbl
      | Or' (exps)                        -> let or_tbl  = List.rev (List.fold_right (fun exp -> getting_free_vars exp) exps []) in
                                             tbl @ or_tbl
      | Applic' (op, rands)               -> let op_tbl = getting_free_vars op tbl in
                                             let applic_tbl =  List.rev (List.fold_right (fun exp -> getting_free_vars exp) rands []) in
                                             op_tbl @ applic_tbl
      | ApplicTP' (op, rands)             -> let op_tbl = getting_free_vars op tbl in 
                                             let applic_tbl = List.rev (List.fold_right (fun exp -> getting_free_vars exp) rands []) in 
                                             op_tbl @ applic_tbl
      | Var'(VarFree x)                   -> tbl@[x]
      | Var'(var)                         -> []
      | Set' (var,exp)                    -> getting_free_vars exp tbl
      | LambdaSimple'(vars, body)         -> getting_free_vars body tbl
      | LambdaOpt'(vars,opt_vals, body)   -> getting_free_vars body tbl

  let rec make_tupple_fvar index tbl= 
    match tbl with 
      | []       -> []
      | hd::tail -> (hd,(index,"MAKE_UNDEF")) :: make_tupple_fvar (index + 1) tail    


  let make_consts_tbl asts =
    let tbl_default = [Void; Sexpr(Nil);Sexpr(Bool false);Sexpr(Bool true)] in
    let tbl_const = List.flatten (List.map (fun ast -> collect_the_sexprs ast []) asts) in 
    let tbl_const = tbl_default @ tbl_const in 
    let tbl_set = removing_duplicates tbl_const in
    let tbl_sub_constants = serch_for_all_sub_constants tbl_set in
    let tbl_set_sub_constants = removing_duplicates tbl_sub_constants in
    let tbl_constants = populate_constants_table tbl_set_sub_constants tbl_set_sub_constants  in
    let tbl_constants = make_final_tupple tbl_constants tbl_constants in
    tbl_constants;;  

  let make_fvars_tbl asts = 
    let tbl_fvar = List.flatten (List.map (fun ast -> getting_free_vars ast []) asts) in
    let tbl_set_fvars = removing_duplicates tbl_fvar in 
    let tbl_tupple = make_tupple_fvar 0 tbl_set_fvars in   
    tbl_tupple

 
  let label_in_fvar_table name fvars_tbl = let (index,_) = (List.assoc name fvars_tbl) in
                                           Printf.sprintf ("fvar_tbl + %d *WORD_SIZE") index 

  let rec generate consts_tbl fvars_tbl e = 
    match e with
      | Const'(exp)        ->  let (offset,_) = List.assoc exp consts_tbl in
                               Printf.sprintf ("mov rax, const_tbl+%d\n") offset 
      | Seq'(exps)         ->  String.concat "\n" (List.map (fun exp -> (generate consts_tbl fvars_tbl exp)) exps)            
      | If'(test,dit,dif)  -> let lelse_label =  label_maker "Lelse" in
                              let lexit_label =  label_maker "Lexit" in
                              (generate consts_tbl fvars_tbl test) ^
                              "cmp rax, SOB_FALSE_ADDRESS \n" ^
                              "je " ^lelse_label^ "\n"
                              ^ (generate consts_tbl fvars_tbl dit) ^
                              "jmp " ^lexit_label^ "\n"
                              ^ lelse_label^ ":\n"
                              ^ (generate consts_tbl fvars_tbl dif) ^
                              lexit_label^ ":\n"
      | Or' (exps)         ->  String.concat "\n" (List.map (fun exp -> (generate consts_tbl fvars_tbl exp) ^ "cmp rax, SOB_FALSE_ADDRESS \n" ^
                                "jne Lexit \n"  ) exps) 
                                ^ "Lexit:\n" 
      | Var'(VarParam(_, minor)) -> Printf.sprintf ("mov rax, qword [rbp + 8 ∗ (4 + %d)")  minor
      | Var'(VarBound(_, major, minor)) -> "mov rax, qword [rbp + 8 ∗ 2]\n" ^
                                            (Printf.sprintf ("mov rax, qword [rax + 8 ∗ %d]\n") major) ^
                                             Printf.sprintf (" mov rax, qword [rax + 8 ∗ %d]\n") minor

      | Var'(VarFree(v)) ->  "mov rax, qword ["^ (label_in_fvar_table v fvars_tbl) ^ "]\n"
      | Set'(Var'(VarParam(_, minor)),exp) -> (generate consts_tbl fvars_tbl exp) ^
                                              (Printf.sprintf ("mov qword [rbp + 8 ∗ (4 + %d)], rax\n") minor) ^
                                              "mov rax,SOB_VOID_ADDRESS\n"
      | Set'(Var'(VarBound(_,major,minor)) , exp) ->  (generate consts_tbl fvars_tbl exp) ^ 
                                                      "mov rax, qword [rbp + 8 ∗ 2]\n" ^
                                                      (Printf.sprintf ("mov rbx, qword [rbx + 8 ∗ %d]\n") major) ^
                                                      (Printf.sprintf ("mov qword [rbx + 8 ∗ %d], rax\n") minor) ^
                                                      "mov rax,SOB_VOID_ADDRESS\n"

      | Set'(Var'(VarFree(v)), exp) -> (generate consts_tbl fvars_tbl exp) ^
                                       ("mov qword [" ^ (label_in_fvar_table v fvars_tbl) ^ "], rax\n") ^ 
                                       "mov rax,SOB_VOID_ADDRESS\n"

       | Def' (Var'(VarFree(v)), exp) -> (generate consts_tbl fvars_tbl exp) ^
                                       ("mov qword [" ^ label_in_fvar_table v fvars_tbl ^ "], rax\n")  ^ 
                                       "mov rax,SOB_VOID_ADDRESS\n"                                                                                                           

      | _                  ->  "dummy";;
     
end;;
(* 
  let test1 = Code_Gen.make_consts_tbl (Semantics.run_semantics (Tag_Parser.tag_parse_expression (Reader.read_sexpr
    "(lambda (a b . c)
      (lambda ()
        (set! a (+ a \'1))
        2
      )
      (lambda () (set! b (lambda () (set! c 1))))
    )")));;


      let test4 = Code_Gen.make_consts_tbl (Semantics.run_semantics (Tag_Parser.tag_parse_expression (Reader.read_sexpr
    "(list \"ab\" \'(1 2) \'c \'ab)"))) ;;


        let test5 = Code_Gen.make_fvars_tbl (Semantics.run_semantics (Tag_Parser.tag_parse_expression (Reader.read_sexpr
    "(lambda () (list c b) (+ c 2) c c"))) ;;


      let test6 = Code_Gen.make_fvars_tbl (Semantics.run_semantics (Tag_Parser.tag_parse_expression (Reader.read_sexpr
    "(lambda (a b . c)
      (lambda ()
        (set! a (+ d \'1))
        2
      )
      (lambda () (set! b (lambda () (set! c 5))))
    )")));; *)